(ns prolingen.vortrag
  (:require [clojure.spec.alpha :as s]))

;;; Stack

(defn- make-stack []
  (fn [] nil))

(defn- pop-stack [stack]
  (let [list (stack)]
    (fn [] (rest list))))

(defn- peek-stack [stack]
  (let [list (stack)]
    (first list)))

(defn- push-stack [stack var]
  (let [list (stack)]
    (fn [] (conj list var))))

;;; JOB-SYSTEM

(defn new-job [type aux]
  {::type type,
   ::aux aux})

(defn return
  "Return from job."
  ([acc] (return acc []))
  ([acc job-list]
   {:acc acc,
    :jobs (vec job-list)}))

(defmulti do-job! (fn [job input] (get job ::type)))

(defn should-stop? [stops data]
  (contains? stops (first (:jobs data))))

(defn initial-data
  "Bundles the job queue and the acc into one data structure for easier handling."
  ([jobs] (initial-data jobs nil))
  ([jobs acc] {:acc acc, :jobs jobs}))

(defn process-next-job [data]
  (let [acc   (:acc data)
        jobs  (:jobs data)
        job   (first jobs)
        to-do (vec (rest jobs))]
    (if (or (nil? job)
            (= :error job))
      (assoc data :jobs nil)
      (let [{new-acc :acc,
             new-jobs :jobs} (do-job! job acc)]
        ;; We deliberately "overwrite" the keys in data here instead of creating
        ;; a new map here, since we want to support nonstandard "data"
        ;; that has additional keys needed for other purposes.
        (-> data
            (assoc :acc   new-acc)
            (assoc :jobs (into new-jobs to-do)))))))

(defn no-jobs?
  "Checks wether data still contains jobs that need to be handled."
  [data]
  (or (nil? (:jobs data))
      (= [] (:jobs data))))

(defn initial-step-data [jobs]
  (assoc (initial-data jobs) :past (make-stack)))

(defn step-forwards [data]
  (let [new-data (process-next-job data)]
    (update new-data :past push-stack (dissoc data :past))))

(defn step-backwards [data]
  (if-let [old-data (peek-stack (:past data))]
    (assoc old-data :past (pop-stack (:past data)))
    data))

(defn job-debug-continue
  ([continue-data] (job-debug-continue continue-data #{::error}))
  ([continue-data stops]
   (loop [data continue-data]
     (cond
       (no-jobs? data) data
       (should-stop? stops data) (update data :jobs #(-> % rest vec))
       ;; This has to be done like this since
       ;; you cannot recur inside a try
       :else (let [new-data (try
                              (step-forwards data)
                              (catch Exception e
                                (do
                                  (println "[ERROR] CAUGHT EXCEPTION")
                                  (assoc data :exception e))))]
               (if (:exception new-data)
                 new-data
                 (recur new-data)))))))

(defn step-marker
  ([data] (step-marker data ::halt))
  ([data marker] (job-debug-continue data #{::error marker})))

(defn job-debug-loop [starting-jobs]
  (job-debug-continue (initial-step-data starting-jobs)))

;;; MACRO

(defmacro defjob* [job job-arg acc-arg [& args] & body]
  (let [qualified (keyword (name (ns-name *ns*)) (str job))
        aux-map   (reduce (fn [acc x] (assoc acc (keyword x) x)) {} args)
        ;; its actually a vector
        reverse-aux-map (reduce (fn [acc x] (into acc [x `(-> ~job-arg ::aux ~(keyword x))])) [] args)]
    `(do (defn ~job [~@args]
           (new-job ~qualified ~aux-map))
         ;; we actually want there to be a clash with acc and ~@body
         (defmethod do-job! ~qualified [~job-arg ~acc-arg]
           (let ~reverse-aux-map
             ~@body)))))

(defmacro defjob "Like `defjob*` but with predefined `job-arg` and `acc-arg`."
  [job [& args] & body] `(defjob* ~job job# ~(symbol "acc") [~@args] ~@body))

;;; JOBS

(defjob print-job [string] (println string) (return acc []))
(defjob error-job [string values] (return acc [(print-job (str "[ERROR] " string)) ::error]))
(defjob update-acc-job [update] (return (update acc) []))

#_(defn print-job
  "`string` is a string that is supposed to be printed."
  [string]
  (new-job  ::print {:string string}))

#_(defn error-job
  "`string` is a string describing the error. `values` is data that can help the programmer
  find out whats wrong (its not used logic wise)."
  ([string] (error-job string nil))
  ([string values] (new-job ::error-msg {:string string,
                                         :values values})))

#_(defmethod do-job! ::print [job acc]
  (let [string (-> job ::aux :string)]
    (println string)
    (return acc [])))

#_(defmethod do-job! ::error-msg [job acc]
  (let [string (-> job ::aux :string)
        pjob   (print-job (str "[ERROR] " string))]
    (return acc [pjob ::error])))

(defn setup-cuts [or-job jobs]
  (let [level (:level or-job)]
    (mapv #(if (= ::cut-job (::type %))
             (assoc % ::aux level)
             %) jobs)))

(defjob* or-job job acc [options] (return acc (setup-cuts job (first options))))
(defjob cut-job [] (return acc []))

;;; OR ENABLED LOOP
(defn next-job [data]
  (first (:jobs data)))

(defn encountered-error? [data]
  (= ::error (next-job data)))

(defn set-level [jobs level]
  (mapv #(if (and (map? %)
                  (nil? (:level %)))
           (assoc % :level level) %) jobs))

(defn or-data [starting-jobs]
  (-> (initial-step-data (set-level starting-jobs 0))
      (assoc :direction :FORWARD)))
(defn process-next-job' [data]
  (let [acc   (:acc data)
        jobs  (:jobs data)
        job   (first jobs)
        level (:level job)
        to-do (vec (rest jobs))]
    (if (or (nil? job)
            (= :error job))
      (assoc data :jobs nil)
      (let [{new-acc :acc,
             new-jobs :jobs} (do-job! job acc)
            new-jobs' (set-level new-jobs (inc level))]
        ;; We deliberately "overwrite" the keys in data here instead of creating
        ;; a new map here, since we want to support nonstandard "data"
        ;; that has additional keys needed for other purposes.
        (-> data
            (assoc :acc   new-acc)
            (assoc :jobs (into new-jobs' to-do)))))))
(defn or-step-forwards [data]
  (let [new-data (process-next-job' data)]
    (update new-data :past push-stack (dissoc data :past))))

(defn- set-cutlevel [data level]
  (if (:cut-level data)
    (update data :cut-level #(min level %))
    (assoc data :cut-level level)))

(defn- should-skip? [or-job cutlevel]
  (let [level (:level or-job)]
    (when cutlevel
      (>= level cutlevel))))

(defn or-process-next [data]
  (case (:direction data)
    :FORWARD (cond
               (no-jobs? data) (assoc data :end? true)
               (encountered-error? data) (do
                                           #_(println "START BACKTRACKING")
                                           (assoc data :direction :BACKWARDS))
               :else (let [new-data (try
                                      (or-step-forwards data)
                                      (catch Exception e
                                        (do
                                          (println "[ERROR] CAUGHT EXCEPTION")
                                          (assoc data :exception e))))]
                       new-data))
    :BACKWARDS (let [cutlevel (:cut-level data)
                     new-data (assoc (step-backwards data) :cut-level cutlevel)
                     next-job (next-job new-data)]
                 (cond
                   (= new-data data) (do #_(println "[ERROR] Didnt work out")
                                         (assoc new-data :end? true))

                   (= (::type next-job) ::cut-job) (-> new-data
                                                       (set-cutlevel (::aux next-job))
                                                       (assoc :direction :BACKWARDS))
                   (and (= (::type next-job) ::or-job)
                        (not (should-skip? next-job (:cut-level new-data)))
                        (> (count (get-in next-job [::aux :options])) 1))
                   (let [changed-data (update-in new-data [:jobs 0 ::aux :options] rest)]
                     (print "END BACKTRACKING")
                     (-> changed-data
                         (assoc  :direction :FORWARD)
                         (dissoc :cut-level)))
                   :else (assoc new-data :direction :BACKWARDS)))))

(defn with-or-continue
  ([continue-data] (with-or-continue continue-data #{::halt}))
  ([continue-data stops]
   (loop [data continue-data]
     (if (or (:end? data)
             (:exception data))
       (dissoc data :end?)
       (recur (or-process-next data))))))

;;; We want try/catch to work like (try;catch), !, continue
(defjob try-job [try catch]
  ;; cut doesnt work outside an or-job yet
  (return acc [(or-job [(or-job try catch) (cut-job)])]))


;;; EIN BISSCHEN PROLOG (IN CLOJURE)

(def term?      vector?)
(def var?       keyword?)
(defn atom? [x] (not (or (term? x)
                         (var?  x))))
(defn type [x]
  (cond
    (var? x)  :VAR
    (term? x) :TERM
    :else     :ATOM))

(defjob start-prolog-job [] (return {:old-acc acc,
                                     :knowledge {}} []))
(defjob quit-prolog-job [transform] (return (transform (:old-acc acc)
                                                       (:knowledge acc)) []))

(defn expand-binding [lhs rhs val]
  (cond
    (term? val) (mapv #(expand-binding lhs rhs %) val)
    (var?  val) (if (= val lhs) rhs val)
    :else       val))
(defn add-binding [acc lhs rhs]
  (if (= lhs :_)
    acc
    (let [knowledge (:knowledge acc)
          updated   (-> (reduce-kv (fn [a var val] (assoc a var (expand-binding lhs rhs val))) {} knowledge)
                        (assoc lhs rhs))]
      (assoc acc :knowledge updated))))

(defn get-binding [acc var]
  (get-in acc [:knowledge var]))

(defn has-binding? [acc var]
  (contains? (:knowledge acc) var))

(defjob occurs-check-job [lhs rhs]
  (cond
    (atom? rhs) (return acc [])
    (var?  rhs) (cond
                  (= lhs rhs)            (return acc [(error-job "Occurs check error" {:lhs lhs, :rhs rhs,
                                                                                       :knowledge (:knowledge acc)})])
                  (has-binding? acc rhs) (let [binding (get-binding acc rhs)]
                                           (return acc [(occurs-check-job lhs binding)]))
                  :else                  (return acc []))
    (term? rhs) (let [jobs (mapv #(occurs-check-job lhs %) rhs)]
                  (return acc jobs))))

(defjob unify-job [lhs rhs]
  (cond
    (and (atom? lhs) (atom? rhs)) (if (= lhs rhs)
                                    (return acc [])
                                    (return acc [(error-job "Unification error (atom mismatch)" {:lhs lhs, :rhs rhs,
                                                                                                 :type [:ATOM :ATOM],
                                                                                                 :knowledge (:knowledge acc)})]))
    (and (var? lhs) (var? rhs) (= lhs rhs)) (return acc [])
    (var? lhs) (if-let [binding (get-binding acc lhs)]
                 (return acc [(unify-job binding rhs)])
                 (return acc [(occurs-check-job lhs rhs)
                              (update-acc-job #(add-binding % lhs rhs))]))
    (var? rhs) (if-let [binding (get-binding acc rhs)]
                 (return acc [(unify-job lhs binding)])
                 (return acc [(occurs-check-job rhs lhs)
                              (update-acc-job #(add-binding % rhs lhs))]))
    (and (term? lhs) (term? rhs)) (let [jobs (mapv unify-job lhs rhs)]
                                    (return acc jobs))
    :else     (return acc [(error-job "Unification error (type mismatch)" {:lhs lhs, :rhs rhs,
                                                                           :type [(type lhs) (type rhs)],
                                                                           :knowledge (:knowledge acc)})])))
;;; is-job as in LHS is FN(vars)
(defn finalized?
  "Checks wether a term does not contain any unbound variables"
  [term]
  (cond
    (atom? term) true
    (term? term) (every? finalized? term)
    :else        false))

(defn expand-once [acc term]
  (let [knowledge (:knowledge acc)]
    (cond
      (var? term) (get knowledge term term)
      (term? term) (mapv #(if (keyword? %) (get knowledge % %) %) term)
      :else term)))

(defjob is-job [lhs f var-list]
  (let [args (mapv #(expand-once acc %) var-list)]
    (if (every? finalized? args)
      (return acc [(unify-job lhs (apply f args))])
      (return acc [(error-job "Some arguments are not fully instantiated." {:lhs lhs,
                                                                            :f f,
                                                                            :var-list var-list,
                                                                            :knowledge (:knowledge acc)})]))))

#_(defjob rule-job [name args & body]
  (let [arity (count args)
        name+arity (keyword (str name) (str arity))
        f (fn [& input]
            )
        ]
    (return (assoc-in acc [:functions name+arity] f))))







;;; NICE INTERFACE



(s/def ::term  (s/or ::var  var?
                     ::atom atom?
                     ::composite (s/coll-of ::term)))
#_(s/def ::form  (s/cat :special ::special :terms (s/* ::term)))
(s/def ::choice (s/cat :start #{'|} :forms (s/+ ::form)))
(s/def ::or-form (s/cat :or #{'or} :first-choice (s/+ ::form) :rest-choices (s/* ::choice)))
(s/def ::eq-form (s/cat :eq #{'=} :lhs ::term :rhs ::term))
(s/def ::fun (s/and symbol?
                    #(not (contains? #{'or, '=} %))))
(s/def ::fun-form (s/cat :fun ::fun :args (s/* ::term)))
(s/def ::cut-form #{'!})
(s/def ::write-form (s/cat :write #{'write} :to-write ::term))
(s/def ::is-form (s/cat :is #{'is} :lhs ::term :rhs (s/and (s/cat :fun any? :args (s/* ::term)))))
(s/def ::nl-form #{'nl})
(s/def ::form (s/or :or    ::or-form
                    :cut   ::cut-form
                    :eq    ::eq-form
                    :write ::write-form
                    :is    ::is-form
                    :fun   ::fun-form
                    :nl    ::nl-form))
(s/def ::input (s/cat :start #{:-} :forms (s/+ ::form)))

(defmulti form->useable first)
(defn term->useable   [term]
  (if (= (first term) ::composite)
    (mapv term->useable (second term))
    (second term)))
(defn choice->useable [choice]
  (mapv form->useable (:forms choice)))
(defmethod form->useable :or [form]
  (let [or-form (second form)
        first-choice (mapv form->useable (:first-choice or-form))
        rest-choices (mapv choice->useable (:rest-choices or-form))
        choices (into [first-choice] rest-choices)]
    {:type :or,
     :choices choices}))
(defmethod form->useable :eq [form]
  (let [eq-form (second form)
        lhs     (term->useable (:lhs eq-form))
        rhs     (term->useable (:rhs eq-form))]
    {:type :eq,
     :lhs  lhs,
     :rhs  rhs}))
(defmethod form->useable :fun [form]
  (let [fun-form (second form)
        args (mapv term->useable (:args fun-form))]
    {:type :fun,
     :fun  (:fun form),
     :args args}))
(defmethod form->useable :cut [form]
  (let [cut-form (second form)]
    {:type :cut}))
(defmethod form->useable :write [form]
  (let [write-form (second form)
        to-write (term->useable (:to-write write-form))]
    {:type :write,
     :to-write to-write}))
(defmethod form->useable :is [form]
  (let [is-form (second form)
        lhs (term->useable (:lhs is-form))
        fun (eval (get-in is-form [:rhs :fun]))
        args (mapv term->useable (get-in is-form [:rhs :args]))]
    {:type :is,
     :lhs lhs,
     :fun fun,
     :args args}))
(defmethod form->useable :nl [form]
  (let [cut-form (second form)]
    {:type :nl}))

(defmulti useable->job :type)
(defmethod useable->job :or    [useable]
  (let [choices (:choices useable)
        job-lists (for [choice choices]
                    (mapv useable->job choice))]
    (or-job (vec job-lists))))
(defmethod useable->job :eq    [useable]
  (unify-job (:lhs useable)
             (:rhs useable)))
(defmethod useable->job :fun   [useable]
  (assert false))
(defmethod useable->job :cut   [useable]
  (cut-job))
(defmethod useable->job :write [useable]
  (is-job :_ print [(:to-write useable)]))
(defmethod useable->job :is    [useable]
  (is-job (:lhs useable)
          (:fun useable)
          (:args useable)))
(defmethod useable->job :nl    [useable]
  (is-job :_ println []))

(defn input->useable [input]
  (let [forms (:forms input)]
    (mapv form->useable forms)))

(defn useable->jobs [useable]
  (mapv useable->job useable))
(defn input->jobs [input ret]
  [(start-prolog-job) (or-job [(-> input input->useable useable->jobs)]) (quit-prolog-job ret)])
(defn unformed->jobs
  ([unformed] (unformed->jobs unformed (fn [old-acc knowledge] old-acc)))
  ([unformed ret] (input->jobs (s/conform ::input unformed) ret)))
(defn unformed->data
  ([unformed] (unformed->data unformed (fn [old-acc knowledge] old-acc)))
  ([unformed ret] (or-data (unformed->jobs unformed ret))))

(defn run-prolog
  ([unformed] (run-prolog unformed (fn [old-acc knowledge] old-acc)))
  ([unformed ret]
   (let [input (s/conform ::input unformed)
         jobs  (input->jobs input ret)
         data  (or-data jobs)]
     (with-or-continue data))))

;;; BEISPIELE

#_(run-prolog '(:- (is :y (+ 1 1))) #(identity %2))
#_(run-prolog '(:- (= :x 1) (or (= :y 2) | (= :y 3)) (= :z 4) (is :z (+ :x :y))) #(identity %2))
#_(run-prolog '(:- (= :x 1) (or (= :y 2) | (= :y 3)) (is :z (vector :x :y)) (= [:_ 3] :z)) #(identity %2))
